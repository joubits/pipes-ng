import { Component } from '@angular/core';
import { match } from 'minimatch';
import { setTimeout } from 'timers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  name = 'Jou Aspiazu';
  numArray:number[] = [0,2,4,6,8,10,12,14,16,18,20];
  PI = Math.PI;
  percentage = 0.356;
  salary = 3550.4;

  persona = {
    'nombre': 'Joe Aspiazu',
    'email': 'joujonas@gmail.com',
    'address': 'Colombus street',

  }

  promiseValue = new Promise ( (resolve, reject) => {
    setTimeout(() => resolve('The data is here...'), 3500);    
  } )
}
